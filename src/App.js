import Border from "./components/Border";
import Color from "./components/Color";
import FontSize from "./components/FontSize";
import "./style.css";
function App() {
  return (
    <div>
      <Border title="Primeiro teste">
        <Color>
          <p>Primeiro componente</p>
        </Color>
      </Border>
      <Border title="Segundo teste">
        <FontSize>
          <p>Segundo componente</p>
        </FontSize>
      </Border>
    </div>
  );
}

export default App;
