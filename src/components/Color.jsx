import { Component } from "react";

class Color extends Component {
  render() {
    return <div className="Color">{this.props.children}</div>;
  }
}
export default Color;
