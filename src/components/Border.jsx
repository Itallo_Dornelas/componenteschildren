import { Component } from "react";

class Border extends Component {
  render() {
    return (
      <div className="Border">
        <h1>{this.props.title}</h1>
        {this.props.children}
      </div>
    );
  }
}
export default Border;
