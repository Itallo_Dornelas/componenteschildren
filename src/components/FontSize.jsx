import { Component } from "react";

class FontSize extends Component {
  render() {
    return <div className="FontSize">{this.props.children}</div>;
  }
}
export default FontSize;
